ref='https://g.co/bard/share/ebb7a49f6edc'

SH=`cd $(dirname ${BASH_SOURCE:-$0}) && pwd`
cd $SH

set -e  # halt if err
set -x  # print the executed

SSH_PUBKEY_S="`cat $SH/sshkey/id_rsa.pub`" \
P=${P:-220} \
  docker-compose build

echo
SSH_PUBKEY_S="`cat $SH/sshkey/id_rsa.pub`" \
P=${P:-220} \
  docker-compose up -d

echo
SSH_PUBKEY_S="`cat $SH/sshkey/id_rsa.pub`" \
P=${P:-220} \
  docker-compose ps

echo
echo "--- ssh command to connect to this container
ssh-add $SH/sshkey/id_rsa ; ssh -i $SH/sshkey/id_rsa root@localhost -oStrictHostKeyChecking=no -oStrictHostKeyChecking=no -p${P:-220}
"

#TODO make it multi-intances ready when run this composeup
