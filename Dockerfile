FROM ubuntu

RUN apt update && apt install -y openssh-server

RUN mkdir /run/sshd
RUN chown $USER:$USER /run/sshd

ARG SSH_PUBKEY_S
RUN test -n "$SSH_PUBKEY_S"

RUN mkdir -p               $HOME/.ssh/
RUN echo "$SSH_PUBKEY_S" > $HOME/.ssh/authorized_keys
RUN chmod 600              $HOME/.ssh/authorized_keys

EXPOSE 22
CMD /usr/sbin/sshd -D
